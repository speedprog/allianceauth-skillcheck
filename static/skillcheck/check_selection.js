(function() {
	function handleShowSkills(event) {
		let select = document.getElementById('showskills-charid');
		window.location = select.value;
	}
	
	function init() {
		$(document).on('click', '[data-action="show-skills"]', handleShowSkills);
	}
	$(document).ready(init);
})();