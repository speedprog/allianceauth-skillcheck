(function() {
	function handleRemove(event) {
		let target = event.currentTarget;
		let url = target.getAttribute('data-url');
		$.post({
			'url': url
		}).then(
		function(data, textStatus, jqXHR) {
			let li_element = document.getElementById(`skillset-${skillset_id}`);
			li_element.remove();
		});
	}
	
	function init() {
		$(document).on('click', '[data-action="update-skills"]', handleRemove);
	}
	$(document).ready(init);
})();