(function() {
	function handleRemove(event) {
		let target = event.currentTarget;
		let url = target.getAttribute('data-url');
		let col_id = target.getAttribute('data-id');
		$.post({
			'url': url,
			'data': {'collection_id': col_id}
		}).then(
		function(data, textStatus, jqXHR) {
			let li_element = document.getElementById(`collection-${col_id}`);
			li_element.remove();
		});
	}
	
	function init() {
		$(document).on('click', '[data-action="remove-col"]', handleRemove);
	}
	$(document).ready(init);
})();