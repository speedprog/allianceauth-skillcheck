from __future__ import unicode_literals
from django.conf.urls import url, include
from . import views

app_name = 'skillcheck'

local_urlpatterns = [
    url(r'^$', views.view_index, name='view_index'),

    url(r'^skillsets/add/$', views.skillset_add, name='skillset_add'),
    url(r'^skillsets/remove/$', views.skillset_remove, name='skillset_remove'),
    url(r'^skillsets/(?P<skillset_id>\d+)/add_skill/$',
        views.skillset_add_skill_view,
        name='skillset_add_skill_view'),
    url(r'^skillsets/(?P<skillset_id>\d+)/remove_skill/$',
        views.skillset_remove_skill,
        name='skillset_remove_skill'),
    url(r'^skillsets/(?P<skillset_id>\d+)/$', views.skillset_edit_view,
        name='skillset_edit_view'),
    url(r'^skillsets/$', views.skillsets_view, name='skillsets_view'),

    url(r'^characters/(?P<character_id>\d+)/update/$',
        views.character_skills_update,
        name='character_skills_update'),
    url(r'^characters/(?P<character_id>\d+)/show/$',
        views.character_skills_show_view,
        name='character_skills_show_view'),

    url(r'^collections/view/$', views.collections_view,
        name='collections_view'),
    url(r'^collections/add/$', views.collection_add,
        name='collection_add'),
    url(r'^collections/remove/$', views.collection_remove,
        name='collection_remove'),
    url(r'^collections/(?P<collection_id>\d+)/edit/$',
        views.collection_edit_view,
        name='collection_edit_view'),
    url(r'^collections/(?P<collection_id>\d+)/add_set/$',
        views.collection_add_set,
        name='collection_add_set'),
    url(r'^collections/(?P<collection_id>\d+)/remove_set/$',
        views.collection_remove_set,
        name='collection_remove_set'),

    url(r'^check/view_result/$', views.check_character_view,
        name='check_character_view'),
    url(r'^check/select/$', views.check_character_selection_view,
        name='check_character_selection_view'),

    url(r'^download/skill_list$', views.download_skills,
        name='download_skills'),
]

urlpatterns = [
    url(r'^skillcheck/', include((local_urlpatterns, 'skillcheck'))),
]
